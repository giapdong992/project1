/**
 * Copyright 2018, Google LLC.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * link: https://github.com/GoogleCloudPlatform/nodejs-docs-samples
 * terminal: git clone https://github.com/GoogleCloudPlatform/nodejs-docs-samples
             cd nodejs-docs-samples/appengine/websockets/
 */

'use strict';

// [START appengine_websockets_app]
const app = require('express')();
var fs = require('fs');
var Player = require('./Player.js');


var players = [];
var sockets = [];
var usrname = [];
const server = require('http').Server(app);
const io = require('socket.io')(server);

//const SQL = require('./querysql.js');
//const sql = new SQL('localhost', 'root', '', 'mydb');
//sql.connect();

app.get('/', (req, res) => {
    console.log("Request: " + req.url);
    if (req.url === '/' || req.url.indexOf('environment') > -1)
        fs.readFile('login.html', "UTF-8", function(err, html) {
            res.writeHead(200, { "Content-Type": "text/html" });
            res.end(html);
        });
});
app.get('*.*', function(req, res) {
    console.log("Request: " + req.url);
    fs.readFile(req.url, "UTF-8", function(err, html) {
        res.end(html);
    });
})

//<===============================================\/=======================================================>
//<===============================================\/=======================================================>
//<===============================================\/=======================================================>

app.post('/login', function(req, res) {

    let body = '';
    req.on('data', chunk => {
        body += chunk.toString(); // convert Buffer to string
    });
    req.on('end', function() {
        var username = body.split('=')[1].split('&')[0];
        var password = body.split('=')[2];
        /*
        if (sql.checkLogin(username, password)) {
            fs.readFile('main.html', "UTF-8", function(err, html) {
                res.writeHead(200, { "Content-Type": "text/html" });
                res.end(html);
            });
        }
        */
        checkLogin(username, password, function(e) {
            if (e == true) {
                fs.readFile('main.html', "UTF-8", function(err, html) {
                    res.writeHead(200, { "Content-Type": "text/html" });
                    res.end(html);
                });
            }
        })
    })
})


function checkLogin(username, password, callback) {
    /*
    if ((username == 'giapdong' && password == 'giapdong') ||
        (username == 'admin' && password == 'admin') ||
        (username == 'project1' && password == 'project1')) {
        return true;
    } else return false; */


    sql.checkLogin(username, password, function(e) {
        callback(e);
    })

}

//<===============================================\/=======================================================>
//<===============================================\/=======================================================>
//<===============================================\/=======================================================>

io.on('connection', socket => {
    console.log("Have a connect to server");
    var player = new Player();
    var thisPlayerID = player.id;
    var thisusr = '';
    players[thisPlayerID] = player;
    sockets[thisPlayerID] = socket;

    socket.on('chat message', msg => {
        io.emit('chat message', msg);
    });
    socket.on('register', function(e) {
        usrname[e] = thisPlayerID;
        thisusr = e;
    })
    socket.on('login', function(data) {
        checkLogin(data.username, data.password, function(e) {
            if (e == true) {
                socket.emit('login-success');
            } else socket.emit('login-failed');
        })
    })

    /*
    socket.emit('spawn', player);
    socket.broadcast.emit('spawn', player);

    for (var playerID in players) {
        if (playerID != thisPlayerID) {
            socket.emit('spawn', players[playerID]);
        }
    } */
    socket.on('confirm-user-friend', function(data) {
        var bool = false;
        for (var usr in usrname) {
            if (usr == data.friend) {
                //socket.emit('confirmed-user-friend', true);
                bool = true;
            }
        }
        if (data.friend == '?')
            bool = true;
        socket.emit('confirmed-user-friend', bool);
    })
    socket.on('disconnect', function() {
        console.log("Disconnect to server");
        delete players[thisPlayerID];
        delete sockets[thisPlayerID];

        socket.broadcast.emit('disconnected', player);
    });
    //<===============================================\/=======================================================>
    //<===============================================\/=======================================================>
    //<===============================================\/=======================================================>
    //For main.html
    /*
    socket.on('confirm-user-friend', function(data) {
        if (sql.checkFriend(data.me, data.friend))
            socket.emit('confirmed-user-friend', true);
        else socket.emit('confirmed-user-friend', false);
    })

    socket.on('connect-sql', function() {
        sql.connect();
    })

    socket.on('select-databse', function() {
        sql.selectDatabase('db_project1');
    })

    socket.on('query-sql', function() {
        var query = 'select * from user';
        var results = sql.excuteQuery(query, function(e) {
            socket.emit('queryed', e);
        });

    })
    */
});

if (module === require.main) {
    const PORT = process.env.PORT || 8080;
    server.listen(PORT, () => {
        console.log(`App listening on port ${PORT}`);
        console.log('Press Ctrl+C to quit.');
    });
}
// [END appengine_websockets_app]