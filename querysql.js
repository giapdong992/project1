/* For command line in Google Cloud
 * show databases; => Show all database
 * show tables; => Show all tables in a database
 * 
 * Developer: Winter Epic
 */

const mysql = require('mysql');
class SQL {
    constructor(host, user, password, database) {
        this.conn = mysql.createConnection({
            host: host,
            user: user,
            password: password,
            database: database,
            multipleStatements: true
        })
    }

    connect() {
        this.conn.connect(function(err) {
            if (err) {
                console.log(err);
            } else console.log("Connected!");
        });
    }
    selectDatabase(name) {
        var sql = 'use ' + name;
        this.excuteQuery(sql);
    }
    excuteQuery(sql, callback) {
        this.conn.query(sql, function(err, result) {
            if (err) {
                console.log("Failed  select db")
                return;
            }
            callback(result);
        });
    }

    checkLogin(username, password, callback) {
        var sql = 'select * from users'
        var bool = false;
        this.excuteQuery(sql, function(result) {
            var bool = false;
            for (var i = 0; i < result.length; i++) {
                var element = result[i];
                if (element.username == username && element.password == password) {
                    bool = true;
                    break;
                }
            }
            callback(bool);
        })
    }
    checkFriend(me, friendID) {
        return true;
    }
}

module.exports = SQL;